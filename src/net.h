/*******************************************************************************
 * XeonNC
 *
 * net.h:  Network utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef NET_H
#define NET_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef NET_C
extern void setWireless( char *ptr );
extern void getWireless( char **ptr );
#endif /* !NET_C */
#endif /* !NET_H */
