/*******************************************************************************
 * XeonNC
 *
 * xeonnc.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define XEONNC_C

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>

#include "xeonnc.h" 

/* Local prototypes */
static gboolean draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static gboolean iconTouchGTK( gpointer data );
static void do_drawing( GtkWidget *drarea, int idx, int doHighlight);
static void quitProgram();

cairo_t *cr = NULL;

/* This holds the wifi configuration provided by piboxd */
gchar *wifi_config = NULL;

/* One more here than we need so the last will be NULL */
GtkWidget *darea[45];

GtkWidget *iconTable = NULL;
// guint iconPad = 18;
gint largeFont  = 20;

int currentIdx = 0;

guint homeKey = -1;
int enableTouch = 1;

GdkRectangle displaySize = {0};

/* Keyboard key pages */
// Main key page
int keys1[33] = {
     'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '\\',
     'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'',
     'z', 'x', 'c', 'z', 'b', 'n', 'm', ',', '.', '/', 0x0
};
// Shift page
int keys2[33] = {
     'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '|',
     'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"',
     'Z', 'X', 'C', 'Z', 'B', 'N', 'M', '<', '>', '?', 0x0 
};
// Alphanum page
int keys3[33] = {
     '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-',
     '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_',
     '{', '}', '[', ']', '+', '=', '.', 0x0, 0x0, 0x0, 0x0
};

#if 0
/* These are loosely based on ASCII codes but are not exactly the same. */
#define BK_TAB      0x0009
#define BK_NOOP     0x0000
#define BK_BACK     0x0008
#define BK_SHIFT    0x000f
#define BK_SPACE    0x0020
#define BK_FORWARD  0x0012
#define BK_SUBMIT   0x000D
char keysb[11] = { BK_TAB, BK_NOOP, BK_BACK, BK_NOOP, BK_SHIFT, BK_SPACE, BK_SHIFT, BK_NOOP, BK_FORWARD, BK_NOOP, BK_SUBMIT };
#endif

/* Start on the first keyboard page */
#define PAGE_LOWERCASE  1
#define PAGE_UPPERCASE  2
#define PAGE_ALPHANUM   3
int page_idx=PAGE_LOWERCASE;

char *labels[] = {
    "SSID",
    "Security",
    "PW",
    "Address",
};

int security_mode_idx = 0;
char *security_options[] = {
    "WPA & WPA2 Personal",
    "WPA & WPA2 Enterprise",
    NULL
};

/* Which editable field are we in? There are four text fields and one drop down list. */
GtkWidget *entry[3];
GtkWidget *options;
#define FIELD_SSID      0
#define FIELD_SECURITY  1
#define FIELD_PW        2
#define FIELD_ADDRESS   3
int currentField = 0;

/*
 *========================================================================
 * Name:   submit
 * Prototype:  void submit( void )
 *
 * Description:
 * Grabs the fields and submits the request to piboxd.
 *========================================================================
 */
void
submit()
{
    const gchar *ssid;
    const gchar *pw;
    const gchar *ipaddr;
    gchar       *security;
    gchar       *config;

    ssid     = gtk_entry_get_text(GTK_ENTRY(entry[0]));
    pw       = gtk_entry_get_text(GTK_ENTRY(entry[1]));
    ipaddr   = gtk_entry_get_text(GTK_ENTRY(entry[2]));
    security = gtk_combo_box_get_active_text(GTK_COMBO_BOX(options));

    piboxLogger(LOG_INFO,
                "Fields: \n"
                "ssid    : %s\n"
                "pw      : %s\n"
                "ipaddr  : %s\n"
                "security: %s\n",
                ssid, pw, ipaddr, security);

    if ( (strlen(ssid) > 0) &&
         (strlen(security) > 0) &&
         (strlen(pw) > 0) )
    {
        piboxLogger(LOG_INFO, "Submitting wifi configuration request.\n");
        config=malloc(strlen(ssid) + strlen(security) + strlen(pw) + 3);
        sprintf(config, "%s:%s:%s", ssid, security, pw);
        setWireless(config);
        free(config);

        /* Validate the changes have been made. */
        sleep(2);
        // Retrieve settings and compare.
    }
    else
        piboxLogger(LOG_ERROR, "Missing fields - not submitting wifi configuration request.\n");
}

/*
 *========================================================================
 * Name:   set_active_field
 * Prototype:  void set_active_field( int idx )
 *
 * Description:
 * Set the widget that will accept input.
 *========================================================================
 */
void
set_active_field( int idx )
{
    piboxLogger(LOG_INFO, "Setting active field to %d\n", idx);
    switch(idx)
    {
        case FIELD_SSID:
            gtk_widget_grab_focus(entry[0]);
            break;
        case FIELD_PW:
            gtk_widget_grab_focus(entry[1]);
            break;
        case FIELD_ADDRESS:
            gtk_widget_grab_focus(entry[2]);
            break;
        case FIELD_SECURITY:
            gtk_widget_grab_focus(options);
            break;
    }
}

/*
 *========================================================================
 * Name:   keyboard_press
 * Prototype:  void keyboard_press( int idx )
 *
 * Description:
 * Processes a keyboard press/touch.
 *========================================================================
 */
void
keyboard_press( int idx )
{
    GtkWidget   *drarea;
    const gchar *data;
    char        buf[2];
    gint        position;
    int         *keys;

    /* Bottom row */
    if ( idx > 32 )
    {
        switch(idx)
        {
            case BROW_TAB_IDX:
                piboxLogger(LOG_INFO, "TAB key\n");
                piboxLogger(LOG_INFO, "Current active field: %d\n", currentField);
                if ( currentField < 3 )
                    currentField++;
                else
                    currentField = 0;
                piboxLogger(LOG_INFO, "New active field: %d\n", currentField);
                set_active_field( currentField );
                break;
            case BROW_BACK_IDX:
                piboxLogger(LOG_INFO, "BACK key\n");
                switch (page_idx)
                {
                    case PAGE_LOWERCASE:
                        page_idx = PAGE_ALPHANUM;
                        break;
                    case PAGE_UPPERCASE:
                        page_idx = PAGE_LOWERCASE;
                        break;
                    case PAGE_ALPHANUM:
                        page_idx = PAGE_UPPERCASE;
                        break;
                }
                break;
            case BROW_SHIFT1_IDX:
            case BROW_SHIFT2_IDX:
                piboxLogger(LOG_INFO, "SHIFT key\n");
                switch ( page_idx )
                {
                    case PAGE_LOWERCASE:
                        page_idx = PAGE_UPPERCASE;
                        break;
                    case PAGE_UPPERCASE:
                    case PAGE_ALPHANUM:
                        page_idx = PAGE_LOWERCASE;
                        break;
                }
                break;
            case BROW_SPACE_IDX:
                piboxLogger(LOG_INFO, "SPACE key\n");
                /* Are spaces allowed in any of these fields? */
                switch ( currentField )
                {
                    case FIELD_SECURITY:
                        break;
                    case FIELD_SSID: 
                        break;
                    case FIELD_PW: 
                        break;
                    case FIELD_ADDRESS: 
                        break;
                }
                break;
            case BROW_FORWARD_IDX:
                piboxLogger(LOG_INFO, "FORWARD key\n");
                switch (page_idx)
                {
                    case PAGE_LOWERCASE:
                        page_idx = PAGE_UPPERCASE;
                        break;
                    case PAGE_UPPERCASE:
                        page_idx = PAGE_ALPHANUM;
                        break;
                    case PAGE_ALPHANUM:
                        page_idx = PAGE_LOWERCASE;
                        break;
                }
                break;
            case BROW_EXIT_IDX:
                piboxLogger(LOG_INFO, "EXIT key\n");
                quitProgram();
                return;
                break;
            case BROW_SUBMIT_IDX:
                piboxLogger(LOG_INFO, "SUBMIT key\n");
                submit();
                return;
                break;
        }
    }

    else 
    {
        piboxLogger(LOG_INFO, "Page idx: %d\n", page_idx);
        switch( page_idx )
        {
            case PAGE_LOWERCASE: keys = keys1; break;
            case PAGE_UPPERCASE: keys = keys2; break;
            case PAGE_ALPHANUM:  keys = keys3; break;
            default: keys = keys1; break;
        }

        switch ( currentField )
        {
            case FIELD_SECURITY:
                piboxLogger(LOG_INFO, "Field: security\n");
                break;
            case FIELD_SSID: 
                piboxLogger(LOG_INFO, "Field: ssid\n");
                data = gtk_entry_get_text(GTK_ENTRY(entry[0]));
                sprintf(buf, "%c", keys[idx]);
                position = strlen(data);
                gtk_editable_insert_text (GTK_EDITABLE (entry[0]), buf, -1, &position);
                break;
            case FIELD_PW: 
                piboxLogger(LOG_INFO, "Field: pw\n");
                data = gtk_entry_get_text(GTK_ENTRY(entry[1]));
                sprintf(buf, "%c", keys[idx]);
                position = strlen(data);
                gtk_editable_insert_text (GTK_EDITABLE (entry[1]), buf, -1, &position);
                break;
            case FIELD_ADDRESS: 
                piboxLogger(LOG_INFO, "Field: address\n");
                if ( isdigit( keys[idx] ) || keys[idx] == '.' )
                {
                    data = gtk_entry_get_text(GTK_ENTRY(entry[2]));
                    sprintf(buf, "%c", keys[idx]);
                    position = strlen(data);
                    gtk_editable_insert_text (GTK_EDITABLE (entry[2]), buf, -1, &position);
                }
                break;
        }
    }

    /* Redraw the keyboard */
    piboxLogger(LOG_INFO, "Redrawing the keyboard.\n");
    drarea = darea[idx];
    do_drawing(drarea, idx, 0);
}

/*
 *========================================================================
 * Name:   pwVisibility
 * Prototype:  void pwVisibility( GtkWidget *widget, gpointer window )
 *
 * Description:
 * Changes the visibility of the PW field.
 *========================================================================
 */
void
pwVisibility( GtkWidget *widget, gpointer window )
{
    if ( gtk_entry_get_visibility (GTK_ENTRY(entry[1])) )
        gtk_entry_set_visibility (GTK_ENTRY(entry[1]), FALSE);
    else
        gtk_entry_set_visibility (GTK_ENTRY(entry[1]), TRUE);
}

/*
 *========================================================================
 * Name:   optionSelected
 * Prototype:  void optionSelected( GtkWidget *widget, gpointer window )
 *
 * Description:
 * Takes note of which security option has been selected.
 *========================================================================
 */
void
optionSelected( GtkWidget *widget, gpointer window )
{
    int i;
    gchar *text = gtk_combo_box_get_active_text(GTK_COMBO_BOX(widget));

    for(i=0; security_options[i] != NULL; i++) 
    {
        if (strcmp(security_options[i], text) == 0 )
        {
            security_mode_idx = i;
            piboxLogger(LOG_INFO, "Selected security mode %d\n", i);
            break;
        }
    }
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   getDisplaySize
 * Prototype:  void getDisplaySize( void )
 *
 * Description:
 * Sets the display size to use.
 *========================================================================
 */
void
getDisplaySize()
{
#define DEFW      480
#define DEFH      640

    GdkScreen       *screen;


    displaySize.width  = piboxGetDisplayWidth();
    displaySize.height = piboxGetDisplayHeight();

    if ( (displaySize.width == 0) || (displaySize.height == 0) )
    {
        screen = gdk_screen_get_default();
        displaySize.width  = (gdk_screen_get_width( screen ) > DEFW)?DEFW:gdk_screen_get_width( screen );
        displaySize.height = (displaySize.width==DEFW)?DEFH:gdk_screen_get_height( screen );
    }

}

/*
 *========================================================================
 * Name:   quitProgram
 * Prototype:  void quitProgram( void )
 *
 * Description:
 * Wrapper to gtk_main_quit() that handles call teardown, etc.
 *========================================================================
 */
static void
quitProgram()
{
    // call_end( );
    gtk_main_quit();
}
static gboolean
quitProgramEvent(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    quitProgram();
    return(FALSE);
}

/*
 *========================================================================
 * Name:   iconTouch
 * Prototype:  void iconTouch( int region )
 *
 * Description:
 * Handler for touch location reports. 
 *========================================================================
 */
void
iconTouch( REGION_T *region )
{
    g_idle_add( (GSourceFunc)iconTouchGTK, (gpointer)region );
}

gboolean
iconTouchGTK( gpointer data )
{
    static int          inprogress = 0;
    int                 idx = 0;
    gint                wx=0, wy=0;
    gint                posX, posY;
    REGION_T            *region = (REGION_T *)data;
    GtkRequisition      req;

    piboxLogger(LOG_INFO, "Entered.\n");

    if ( !enableTouch )
    {
        piboxLogger(LOG_INFO, "Touch processing disabled, skipping update\n");
        return TRUE;
    }

    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return TRUE;
    }
    inprogress = 1;
    piboxLogger(LOG_INFO, "Processing touch request.\n");

    /* Find the widget that maps to the absolute coordinates */
    while( darea[idx] != NULL )
    {
        /* Get the absolute screen coordinates of the widget */
        gdk_window_get_origin (gtk_widget_get_window (darea[idx]), &wx, &wy);
        req.width = gdk_window_get_width(darea[idx]->window);
        req.height = gdk_window_get_height(darea[idx]->window);
        piboxLogger(LOG_INFO, "origin, wxh: %d %d  %d / %d\n", wx, wy, req.width, req.height);

        /*
         * Set up depending on if we are using rotation or not.
         * We only support CCW rotation currently.
         */
        if ( isCLIFlagSet( CLI_ROTATE) )
        {
            piboxLogger(LOG_INFO, "Rotation is enabled.\n");
            posY = region->x;
            posX = displaySize.height - region->y;
        }
        else
        {
            posX = region->x;
            posY = region->y;
        }
        piboxLogger(LOG_INFO, "touch point, x=%d y=%d\n", posX, posY);

        /* Test if the event was within the bounds of the widget area. */
        if ( (posX >= wx) && (posX<= (wx+req.width)) )
        {
            if ( (posY >= wy) && (posY <= (wy+req.height)) )
            {
                /* Found widget! */
                piboxLogger(LOG_INFO, "Identified table cell: %d\n", idx);
                currentIdx = idx;
                keyboard_press(idx);
                gtk_widget_queue_draw(iconTable);
                inprogress = 0;
                return(FALSE);
            }
        }
        idx++;
    }
    inprogress = 0;
    piboxLogger(LOG_INFO, "No matching table cell identified for %d / %d\n", region->x, region->y);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    piboxLoadDisplayConfig();

    /* Are we on a touch screen */
    if ( piboxGetDisplayTouch() )
    {
        setCLIFlag(CLI_TOUCH);
        // iconPad = 9;
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, int, int )
 *
 * Description:
 * Updates the poster area of the display.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * int              Numerically, which button to draw.
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 *
 * Bottom row
 * tab x <- x shift space shift x -> x submit
 *
 * 2nd page
 * 1 2 3 4 5 6 7 8 9 0 -
 * ! @ # $ % ^ & * ( ) _
 * { } [ ] + = x x x x x
 * (bottom row)
 *
 * shift page - uppercase except for punctuation
 * x x x x x x x x x x |
 * x x x x x x x x x : "
 * x x x x x x x < > ? x
 * (bottom row)
 *
 * Shift: toggles 1st page and shift page
 * Arrow keys move forward/back in pages
 *========================================================================
 */
void 
do_drawing( GtkWidget *drarea, int idx, int doHighlight)
{
    gint            iwidth, iheight;
    gint            width, height;
    gint            swidth, sheight;
    gint            awidth, aheight;
    gdouble         offset_x, offset_y;
    gboolean        scaled = FALSE;
    GtkRequisition  req;
    GdkPixbuf       *image = NULL;
    GdkPixbuf       *newimage;
    cairo_text_extents_t    extents;
    // gfloat          ri, rs;
    char            buf[16];
    int             *keys;

    cr = gdk_cairo_create(drarea->window);
    req.width = gdk_window_get_width(drarea->window);
    req.height = gdk_window_get_height(drarea->window);
    awidth = (int)(req.width * .98);
    aheight = (int)(req.height * .97);
    piboxLogger(LOG_TRACE2, "window w/h: %d / %d\n", req.width, req.height);
    piboxLogger(LOG_TRACE2, "adjusted window w/h: %d / %d\n", awidth, aheight);

    /*
     * If requested, draw a background highlight
     * The highlight image has been pre-scaled to fit.
     */
    if ( doHighlight == 1 )
    {
        piboxLogger(LOG_TRACE2, "Adding a highlight: %s\n", cliOptions.path[BROW_MASK]);
        iwidth  = gdk_pixbuf_get_width(cliOptions.image[BROW_BUTTON]);
        iheight = gdk_pixbuf_get_height(cliOptions.image[BROW_BUTTON]);
        piboxLogger(LOG_TRACE2, "highlight size: %d / %d\n", iwidth, iheight);

        sheight = aheight;
        swidth = awidth;

#if 0
        /* Compute scaling to keep aspect ratio but fit in the screen */
        ri = (gfloat)((gfloat)iwidth / (gfloat)iheight);
        rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
        if ( rs > ri )
        {
            swidth = (gint)(iwidth * aheight/iheight);
            sheight = aheight;
        }
        else
        {
            swidth = awidth;
            sheight = (gint)(iheight * awidth/iwidth);
        }
        swidth  += iconPad;
        sheight += iconPad;
#endif
        piboxLogger(LOG_TRACE2, "updated highlight size: %d / %d\n", swidth, sheight);

        if ( ! cliOptions.scaled[BROW_MASK] )
        {
            newimage = gdk_pixbuf_scale_simple(cliOptions.image[BROW_MASK], swidth, sheight, GDK_INTERP_BILINEAR);
            g_object_unref(cliOptions.image[BROW_MASK]);
            cliOptions.image[BROW_MASK] = newimage;
            cliOptions.scaled[BROW_MASK] = TRUE;
        }

        /* Positioning */
        offset_x = (req.width - swidth) / 2.0;
        offset_y = (req.height - sheight) / 2.0;
        gdk_cairo_set_source_pixbuf(cr, cliOptions.image[BROW_MASK], offset_x, offset_y);

        cairo_paint(cr);    
    }

    // Or clear the highlight
    else if ( doHighlight == 2 )
    {
        piboxLogger(LOG_TRACE2, "Clearing highlight: %s\n", cliOptions.path[BROW_MASK]);
        width  = gdk_pixbuf_get_width(cliOptions.image[BROW_MASK]);
        height = gdk_pixbuf_get_height(cliOptions.image[BROW_MASK]);

        /* Compute scaling to keep aspect ratio but fit in the screen */
        sheight = aheight;
        swidth = awidth;
#if 0
        ri = (gfloat)((gfloat)width / (gfloat)height);
        rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
        if ( rs > ri )
        {
            swidth = (gint)(width * aheight/height);
            sheight = aheight;
        }
        else
        {
            swidth = awidth;
            sheight = (gint)(height * awidth/width);
        }
#endif

        offset_x = (req.width - swidth) / 2.0;
        offset_y = (req.height - sheight) / 2.0;
        cairo_set_source_rgba(cr, 0.13, 0.13, 0.13, 1.0);
        cairo_rectangle(cr, offset_x, offset_y, (double)swidth, (double)sheight);
        cairo_fill(cr); 
        cairo_paint(cr);    
    }

    /* Load the button icon. */
    switch( idx )
    {
        case BROW_TAB_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_TAB]);
            image = cliOptions.image[BROW_TAB];
            scaled = cliOptions.scaled[BROW_TAB];
            break;

        case BROW_BACK_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_BACK]);
            image = cliOptions.image[BROW_BACK];
            scaled = cliOptions.scaled[BROW_BACK];
            break;

        case BROW_SHIFT1_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_SHIFT]);
            image = cliOptions.image[BROW_SHIFT];
            scaled = cliOptions.scaled[BROW_SHIFT];
            break;

        case BROW_SPACE_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_SPACE]);
            image = cliOptions.image[BROW_SPACE];
            scaled = cliOptions.scaled[BROW_SPACE];
            break;

        case BROW_SHIFT2_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_SHIFT]);
            image = cliOptions.image[BROW_SHIFT];
            scaled = cliOptions.scaled[BROW_SHIFT];
            break;

        case BROW_FORWARD_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_FORWARD]);
            image = cliOptions.image[BROW_FORWARD];
            scaled = cliOptions.scaled[BROW_FORWARD];
            break;

        case BROW_EXIT_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_EXIT]);
            image = cliOptions.image[BROW_EXIT];
            scaled = cliOptions.scaled[BROW_EXIT];
            break;

        case BROW_SUBMIT_IDX:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_SUBMIT]);
            image = cliOptions.image[BROW_SUBMIT];
            scaled = cliOptions.scaled[BROW_SUBMIT];
            break;

        default:
            piboxLogger(LOG_TRACE2, "Icon path: %s\n", cliOptions.path[BROW_BUTTON]);
            image = cliOptions.image[BROW_BUTTON];
            scaled = cliOptions.scaled[BROW_BUTTON];
            break;
    }
    width  = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);
    piboxLogger(LOG_TRACE2, "Icon image: w/h: %d / %d \n", width, height);

    /* Compute scaling to keep aspect ratio but fit in the screen */
    // ri = (gfloat)((gfloat)width / (gfloat)height);
    // rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
    sheight = aheight;
    swidth = awidth;
#if 0
    if ( rs > ri )
    {
        piboxLogger(LOG_TRACE2, "rs > ri\n");
        piboxLogger(LOG_TRACE2, "width: %d, aheight: %d, height: %d\n", width, aheight, height);
        swidth = (gint)(width * aheight/height);
        sheight = aheight;
    }
    else
    {
        piboxLogger(LOG_TRACE2, "rs <= ri\n");
        swidth = awidth;
        sheight = (gint)(height * awidth/width);
    }
#endif
    piboxLogger(LOG_TRACE2, "updated icon size: %d / %d\n", swidth, sheight);
    if ( ! scaled )
    {
        newimage = gdk_pixbuf_scale_simple(image, swidth, sheight, GDK_INTERP_BILINEAR);
        switch( idx )
        {
            case BROW_TAB_IDX:
                g_object_unref(cliOptions.image[BROW_TAB]);
                cliOptions.image[BROW_TAB] = newimage;
                cliOptions.scaled[BROW_TAB] = TRUE;
                break;

            case BROW_BACK_IDX:
                g_object_unref(cliOptions.image[BROW_BACK]);
                cliOptions.image[BROW_BACK] = newimage;
                cliOptions.scaled[BROW_BACK] = TRUE;
                break;

            case BROW_SHIFT1_IDX:
                g_object_unref(cliOptions.image[BROW_SHIFT]);
                cliOptions.image[BROW_SHIFT] = newimage;
                cliOptions.scaled[BROW_SHIFT] = TRUE;
                break;

            case BROW_SPACE_IDX:
                g_object_unref(cliOptions.image[BROW_SPACE]);
                cliOptions.image[BROW_SPACE] = newimage;
                cliOptions.scaled[BROW_SPACE] = TRUE;
                break;

            case BROW_SHIFT2_IDX:
                g_object_unref(cliOptions.image[BROW_SHIFT]);
                cliOptions.image[BROW_SHIFT] = newimage;
                cliOptions.scaled[BROW_SHIFT] = TRUE;
                break;

            case BROW_FORWARD_IDX:
                g_object_unref(cliOptions.image[BROW_FORWARD]);
                cliOptions.image[BROW_FORWARD] = newimage;
                cliOptions.scaled[BROW_FORWARD] = TRUE;
                break;

            case BROW_EXIT_IDX:
                g_object_unref(cliOptions.image[BROW_EXIT]);
                cliOptions.image[BROW_EXIT] = newimage;
                cliOptions.scaled[BROW_EXIT] = TRUE;
                break;

            case BROW_SUBMIT_IDX:
                g_object_unref(cliOptions.image[BROW_SUBMIT]);
                cliOptions.image[BROW_SUBMIT] = newimage;
                cliOptions.scaled[BROW_SUBMIT] = TRUE;
                break;

            default:
                g_object_unref(cliOptions.image[BROW_BUTTON]);
                cliOptions.image[BROW_BUTTON] = newimage;
                cliOptions.scaled[BROW_BUTTON] = TRUE;
                break;
        }
        image = newimage;
    }

    /* Positioning */
    offset_x = (double)(req.width - swidth) / (double)2.0;
    offset_y = (double)(req.height - sheight) / (double)2.0;
    gdk_cairo_set_source_pixbuf(cr, image, offset_x, offset_y);
    cairo_paint(cr);    

    /* Draw the index as text over the button image */
    cairo_set_source_rgb(cr, 0.9, 0.9, 0.9);
    cairo_select_font_face(cr, "Courier",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    /*
     * Select the page of keys to display.
     */
    switch( page_idx )
    {
        case PAGE_LOWERCASE: keys = keys1; break;
        case PAGE_UPPERCASE: keys = keys2; break;
        case PAGE_ALPHANUM:  keys = keys3; break;
        default: keys = keys1; break;
    }

    /*
     * Display a key symbol in the button, unless it's not active in the current keyboard page
     * or on the bottom row, which is handled where the images are loaded.
     */
    if ( idx < 33 )
    {
        if ( keys[idx] != 0x0 )
        {
            memset(buf, 0, 3);
            sprintf(buf, "%c", keys[idx]);

            cairo_set_font_size(cr, largeFont);
            cairo_text_extents(cr, buf, &extents);
            cairo_move_to(cr, req.width/2 - extents.width/2, req.height/2 + extents.height/2);
            cairo_show_text(cr, buf);
        }
    }

    cairo_destroy(cr);
    cr = NULL;
}

/*
 *========================================================================
 * Name:   draw_icon
 * Prototype:  void draw_icon( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * 
 *========================================================================
 */
static gboolean 
draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{      
    GtkWidget       *drarea;
    gint            idx = GPOINTER_TO_INT(user_data);

    // Get drawing area widget
    drarea = darea[idx];

    do_drawing(drarea, idx, 0);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   setHighlight
 * Prototype:  void setHighlight( gint idx )
 *
 * Description:
 * Clears and sets highlights for buttons.
 *========================================================================
 */
void 
setHighlight( int idx )
{
    piboxLogger(LOG_INFO, "Entered setHighlightl\n");

    // Remove highlight from current index.
    piboxLogger(LOG_INFO, "Calling doDrawing %d\n", currentIdx);
    do_drawing(darea[currentIdx], currentIdx, 2);

    // Highlight the newly pressed index.
    piboxLogger(LOG_INFO, "Calling doDrawing idx: %d\n", idx);
    do_drawing(darea[idx], idx, 1);

    currentIdx = idx;
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a click in a button.
 *========================================================================
 */
static gboolean 
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{      
    int idx = GPOINTER_TO_INT(user_data);

    piboxLogger(LOG_INFO, "Button %d: x %f, y %f\n", idx, event->x, event->y);
    switch(event->type) {
        case GDK_BUTTON_PRESS:
            piboxLogger(LOG_INFO, "GDK_BUTTON_PRESS\n");
            if ( event->button == 1 )
            {
                // select active item.
                currentIdx = idx;
                keyboard_press(idx);
                gtk_widget_queue_draw(iconTable);
                return(TRUE);
            }
            break;
        default:
            piboxLogger(LOG_INFO, "! GDK_BUTTON_PRESS\n");
            break;
    }
    return(TRUE);
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Managed keyboard presses in the window.
 *========================================================================
 */
static gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    int idx = currentIdx;
    piboxLogger(LOG_INFO, "Key: %s\n", gdk_keyval_name(event->keyval));
    switch(event->keyval) 
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {   
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                quitProgram();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            quitProgram();
            return(TRUE);
            break;

        case GDK_Left:
        case GDK_KP_Left:
            piboxLogger(LOG_INFO, "Left key\n");
            idx--;
            break;

        case GDK_Right:
        case GDK_KP_Right:
            piboxLogger(LOG_INFO, "Right key\n");
            idx++;
            break;

        case GDK_Up:
        case GDK_KP_Up:
            piboxLogger(LOG_INFO, "Up key\n");
            idx -=3;
            break;

        case GDK_Down:
        case GDK_KP_Down:
            piboxLogger(LOG_INFO, "Down key\n");
            idx +=3;
            break;

        case GDK_Return:
        case GDK_KP_Enter:
            piboxLogger(LOG_INFO, "Return key makes the call.\n");
            idx = 12;
            break;

        default:
            if ( event->keyval == homeKey )
            {   
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                quitProgram();
                return(TRUE);
            }
            else
            {   
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }

    // Clamp the idx to the number of possible buttons.
    if ( idx < 0 )
        idx = 14;
    else if ( idx > 14 )
        idx = 0;

    // Update the display
    setHighlight(idx);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a scrolled list of icons on the left and currently
 * selected app information on the right.  
 *
 * Notes:
 * If compiled with -DXEON then we don't use the splash screen to save 
 * screen space.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *window;
    GtkWidget           *label;
    GtkWidget           *vbox;
    GtkWidget           *dwga;
    gint                row, col, idx, i, j, k;
    guint               numRows;
    guint               numCols;
    gchar               *ssid=NULL, *security=NULL, *pw=NULL;

    if ( wifi_config != NULL )
    {
        ssid = strtok(wifi_config, ":");
        security = strtok(NULL, ":");
        pw = strtok(NULL, ":");
        piboxLogger(LOG_INFO, "ssid: %s, security: %s, pw: %s\n", ssid, security, pw);
    }

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );

    if ( ! piboxGetDisplayTouch() )
    {
        /* Enables keyboard support */
        gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
        g_signal_connect(G_OBJECT(window), 
                "key_press_event", 
                G_CALLBACK(key_press), 
                NULL); 
    }

    /* 
     * We have 5 rows plus the keyboard (which includes command buttons).
     */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_container_add (GTK_CONTAINER (window), vbox);

    /* First a table of configuration options. */
    iconTable = gtk_table_new(7,2,FALSE);
    gtk_table_set_col_spacing (GTK_TABLE(iconTable), 0, 20);
    gtk_box_pack_start (GTK_BOX (vbox), iconTable, TRUE, TRUE, 10);

    for (i=0; i<7; i++)
    {
        switch (i)
        {
            /* SSID */
            case 0:
                label = gtk_label_new(labels[i]);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 0, 1, i, i+1 );
                entry[i] = gtk_entry_new();
                if ( ssid != NULL )
                    gtk_entry_set_text(GTK_ENTRY(entry[i]), ssid);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), entry[i], 1, 2, i, i+1 );
                break;

            /* PW */
            case 2:
                label = gtk_label_new(labels[i]);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 0, 1, i, i+1 );
                entry[i-1] = gtk_entry_new();
                if ( pw != NULL )
                    gtk_entry_set_text(GTK_ENTRY(entry[i-1]), pw);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), entry[i-1], 1, 2, i, i+1 );
                break;

            /* IP Address */
            case 3:
                label = gtk_label_new(labels[i]);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 0, 1, i, i+1 );
                entry[i-1] = gtk_entry_new();
                gtk_table_attach_defaults(GTK_TABLE(iconTable), entry[i-1], 1, 2, i, i+1 );
                break;

            /* Security options menu */
            case 1:
                label = gtk_label_new(labels[i]);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 0, 1, i, i+1 );

                options = gtk_combo_box_text_new();
                k=0;
                for(j=0; security_options[j] != NULL; j++ )
                {
                    gtk_combo_box_text_insert_text(GTK_COMBO_BOX_TEXT(options), j, security_options[j]);
                    if ( security != NULL )
                    {
                        if ( strcasecmp(security, security_options[j]) == 0 )
                            k = j;
                    }
                }
                gtk_table_attach_defaults(GTK_TABLE(iconTable), options, 1, 2, i, i+1 );
                g_signal_connect(G_OBJECT(options), "changed", G_CALLBACK(optionSelected), (gpointer) 0);
                gtk_combo_box_set_active(GTK_COMBO_BOX(options), k);
                break;

            /* Toggle PW visibility */
            case 4:
                label = gtk_check_button_new_with_label ("Show PW");
                g_signal_connect(G_OBJECT(label), "clicked", G_CALLBACK(pwVisibility), (gpointer) 0);
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 1, 2, i, i+1 );
                break;

            default:
                /* HACK: These are just spacers so the keyboard looks right. */
                label = gtk_label_new("");
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 0, 1, i, i+1 );
                label = gtk_label_new("");
                gtk_table_attach_defaults(GTK_TABLE(iconTable), label, 1, 2, i, i+1 );
                break;
        }
    }

    /* Make the PW field invisible */
    gtk_entry_set_visibility (GTK_ENTRY(entry[1]), FALSE);

    /* 
     * We need 4 rows of 11 buttons each in a table.
     */
    numRows = 4;
    numCols = 11;
    piboxLogger(LOG_INFO, "Number of rows/columns: %d / %d\n", numRows, numCols);

    /* Then a table for the keyboard. */
    iconTable = gtk_table_new(numRows,numCols,TRUE);
    gtk_box_pack_start (GTK_BOX (vbox), iconTable, TRUE, TRUE, 0);

    /*
     * A set of icons
     */
    memset(darea, 0, sizeof(darea));
    idx = 0;
    for (row=0; row<numRows; row++)
    {
        for (col=0; col<numCols; col++)
        {
            dwga = gtk_drawing_area_new();
            g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_icon), GINT_TO_POINTER(idx)); 
            if ( ! piboxGetDisplayTouch() )
            {
                gtk_widget_add_events(dwga, GDK_BUTTON_PRESS_MASK);
                g_signal_connect(G_OBJECT(dwga), "button_press_event", G_CALLBACK(button_press), GINT_TO_POINTER(idx)); 
            }
            gtk_table_attach_defaults(GTK_TABLE(iconTable), dwga, col, col+1, row, row+1 );
            darea[idx] = dwga;

            idx++;
        }
    }

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (quitProgramEvent), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), displaySize.width, displaySize.height);
    gtk_window_set_title(GTK_WINDOW(window), "xeonnc");

    return window;
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            quitProgram();
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            quitProgram();
            break;
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget           *window;
    struct sigaction    sa;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {   
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGUSR1.\n", PROG);
        exit(1);
    }

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    validateConfig();

    /* Read environment config for keyboard behaviour */
    loadKeysyms();
    
    /* Dump some configuration status */
    piboxLogger(LOG_INFO, "Verbosity level: %d\n", cliOptions.verbose);
    if ( cliOptions.flags & CLI_LOGTOFILE )
        piboxLogger(LOG_INFO, "Logfile: %s\n", cliOptions.logFile);
    if ( cliOptions.flags & CLI_ROTATE )
        piboxLogger(LOG_INFO, "Rotation enabled.\n");

    /* Get display config information */
    loadDisplayConfig();

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(iconTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }
    else
        piboxLogger(LOG_INFO, "Not a touch screen device.\n");

    /* Retrieve network configuration information */
    getWireless(&wifi_config);
    if ( wifi_config != NULL )
        piboxLogger(LOG_INFO, "Wirless config: %s\n", wifi_config);
    else
        piboxLogger(LOG_ERROR, "No wirless config found.\n");

    // Initialize gtk, create its windows and display them.
    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST ) )
        gtk_rc_parse("data/gtkrc");

    getDisplaySize();
    piboxLogger(LOG_INFO, "display type: %s\n", 
            (piboxGetDisplayType()==PIBOX_LCD)?"LCD":
            (piboxGetDisplayType()==PIBOX_HDMI)?"HDMI":"Unknown");
    piboxLogger(LOG_INFO, "display resolution: %dx%d\n", 
            displaySize.width, displaySize.height);

    window = createWindow();
    gtk_widget_show_all(window);

    gtk_main();
    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxTouchShutdownProcessor();
    }
    piboxLoggerShutdown();
    return 0;
}
