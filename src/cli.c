/*******************************************************************************
 * XeonNC
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>

#include "xeonnc.h"

#define F_CFG           "/etc/xeonnc.cfg"
#define F_ICONS         "/etc/xeonnc/icons"
#define F_ICONS_T       "data/icons"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/* Names of icons files for buttons. */
static char *icon_file[] = {
    "button.png",
    "mask.png",
    "tab.png",
    "back.png",
    "shift.png",
    "space.png",
    "forward.png",
    "exit.png",
    "submit.png"
};

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt, i;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;

                for(i=0; i<BROW_MAX; i++)
                {
                    free(cliOptions.path[i]);
                    cliOptions.path[i] = malloc(strlen(F_ICONS_T) + strlen(icon_file[i]) + 2);
                    sprintf(cliOptions.path[i], "%s/%s", F_ICONS_T, icon_file[i]);
                }
                break;

            /* -r: Assume rotated (CW) display). */
            case 'r':
                cliOptions.flags |= CLI_ROTATE;
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    struct stat stat_buf;
    FILE *fd;
    char buf[MAXBUF];
    char *name;
    char *value;
    int  i;

    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Default icon path */
    for(i=0; i<BROW_MAX; i++)
    {
        cliOptions.path[i] = malloc(strlen(F_ICONS) + strlen(icon_file[i]) + 2);
        sprintf(cliOptions.path[i], "%s/%s", F_ICONS, icon_file[i]);
    }

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;

    // Check that the configuration file exists.
    if ( stat(F_CFG, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(F_CFG, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", F_CFG, strerror(errno));
            return;
        }

        // Each line is name:value pair
        while ( fgets(buf, MAXBUF, fd) != NULL )
        {
            piboxStripNewline(buf);
            name=strtok(buf, ":");
            value=strtok(NULL, ":");
            if ( (name == NULL) || (value==NULL) )
                continue;

            if ( strcmp(name, "verbose") == 0 )
            {
                /* Set debug output level */
                cliOptions.verbose = atoi(value);
            }
            else if ( strcmp(name, "debugfile") == 0 )
            {
                // Set debug file
                cliOptions.logFile = g_strdup(value);
                cliOptions.flags |= CLI_LOGTOFILE;
            }
            else if ( strcmp(name, "rotate") == 0 )
            {
                if ( 1 == atoi(value) )
                {
                    cliOptions.flags |= CLI_ROTATE;
                }
            }
        }
    
        fclose(fd);
    }
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
    struct stat     stat_buf;
    int             i;

    /*
     * Pre-allocate the pixbufs to save time later.
     */
    for(i=0; i<BROW_MAX; i++)
    {
        if ( stat(cliOptions.path[i], &stat_buf) != 0 )
        {
            piboxLogger(LOG_ERROR, "Can't find icon: %s\n", cliOptions.path[i]);
        }
        else
        {
            cliOptions.image[i] = gdk_pixbuf_new_from_file(cliOptions.path[i], NULL);
            cliOptions.scaled[i] = FALSE;
        }
    }
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

