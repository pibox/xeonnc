/*******************************************************************************
 * XeonNC
 *
 * net.c:  Network utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define NET_C

#include <stdio.h>
#include <ctype.h>
#include <glib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <pibox/pmsg.h>

#include "xeonnc.h"

/*
 *========================================================================
 *========================================================================
 * Private functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 *========================================================================
 * Public functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   setWireless
 * Prototype:  void setWireless( char * )
 *
 * Description:
 * Sends the wireless configuration to the piboxd daemon.
 *========================================================================
 */
void
setWireless( char *config )
{
    piboxMsg(MT_NET, MA_SETWIRELESS, 0, NULL, strlen(config), config, NULL, NULL);
}

/*
 *========================================================================
 * Name:   getWireless
 * Prototype:  void getWireless( void )
 *
 * Description:
 * Retrieves the wireless configuration from the piboxd daemon.
 * Format is SSID:Security:PW
 *========================================================================
 */
void
getWireless(char **buf)
{
    piboxLogger(LOG_INFO, "Requesting wireless configuration.\n");
    piboxMsg(MT_NET, MA_GETWIRELESS, 0, NULL, 0, NULL, NULL, buf);
    if ( *buf != NULL )
        piboxLogger(LOG_INFO, "Response: %s\n", *buf);
    else
        piboxLogger(LOG_INFO, "No Response from server.\n");
}

/*
 *========================================================================
 * Name:   getIP
 * Prototype:  char *getIP( void )
 *
 * Description:
 * Retrieves the wireless IP address.
 * To get the wifi interface name:
 * cat /proc/net/wireless | tail -n +3 | cut -f1 -d":"
 * This is necessary until I can update pnc library to return only wifi
 * interface names with getIFList (or similar).
 *========================================================================
 */
void
getIP(char **buf)
{
    char    ifname[128];
    FILE    *pd;

    /* Get the first wifi device. Xeon's only have one. */
    memset(ifname, 0, 128);
    pd = popen("cat /proc/net/wireless | tail -n +3 | cut -f1 -d\":\"", "r");
    fgets(ifname, 127, pd);
    if ( strlen(ifname) == 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find wireless device.\n");
        return;
    }
    piboxLogger(LOG_INFO, "Requesting IP for interface: %s\n", ifname);

    /* Retrieve the IP for the specified device. */
    piboxMsg(MT_NET, MA_GETIP, 0, NULL, strlen(ifname), ifname, NULL, buf);
    if ( *buf != NULL )
        piboxLogger(LOG_INFO, "Response: %s\n", *buf);
    else
        piboxLogger(LOG_INFO, "No Response from server.\n");
}
